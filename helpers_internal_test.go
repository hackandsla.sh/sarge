package sarge

import (
	"os"
	"reflect"
	"strings"
	"testing"
)

func setOSArgs(t *testing.T, args ...string) {
	t.Helper()

	oldArgs := os.Args

	os.Args = append([]string{"progname"}, args...)

	t.Cleanup(func() {
		os.Args = oldArgs
	})
}

func assertEquals(t *testing.T, want, got interface{}) {
	t.Helper()

	if !reflect.DeepEqual(want, got) {
		t.Errorf("expected '%v', got '%v'", want, got)
	}
}

func assertContains(t *testing.T, want, got string) {
	t.Helper()

	if !strings.Contains(got, want) {
		t.Errorf("expect string to contain '%s', got '%s'", want, got)
	}
}
