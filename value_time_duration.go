package sarge

import (
	"time"
)

func (p *Parser) Duration(d *time.Duration, options ...ArgOption) {
	p.Options(d, options...)
}

type durationValue struct {
	ptr *time.Duration
}

func newDurationValue(v interface{}) Value {
	ptr := v.(*time.Duration)
	return &durationValue{ptr}
}

func (d *durationValue) UnmarshalText(b []byte) error {
	v, err := time.ParseDuration(string(b))
	if err != nil {
		return err
	}

	*d.ptr = v

	return nil
}

func (d *durationValue) String() string { return d.ptr.String() }
