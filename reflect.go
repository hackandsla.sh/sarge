package sarge

import "reflect"

func deepCopy(v reflect.Value) reflect.Value {
	cp := reflect.New(v.Type()).Elem()
	cp.Set(v)

	return cp
}

func isStructPointer(v interface{}) bool {
	value := reflect.ValueOf(v)
	if value.Kind() != reflect.Ptr {
		return false
	}

	if value.Elem().Kind() != reflect.Struct {
		return false
	}

	return true
}

func isPointer(v interface{}) bool {
	value := reflect.ValueOf(v)
	return value.Kind() == reflect.Ptr
}

func initNilPointer(v reflect.Value) {
	if v.IsZero() {
		v.Set(reflect.New(v.Type().Elem()))
	}
}

func canElem(v reflect.Value) bool {
	return v.Kind() == reflect.Ptr || v.Kind() == reflect.Interface
}
