SHELL = bash

GOLANGCI_LINT_VERSION = v1.42.1
LSIF_GO_VERSION = latest
JUNIT_VERSION = v0.9.1

remove-merged-branches:
	git fetch --prune
	git branch --merged | egrep -v "(^\*|master)" | xargs git branch -d

gen:
	go generate ./...

test: gen
	go test ./...

lint:
	go run github.com/golangci/golangci-lint/cmd/golangci-lint@${GOLANGCI_LINT_VERSION} run ./... -v --timeout 5m

tidy:
	./build/tidy.sh

pull-master:
	git checkout master
	git pull

release: pull-master test lint tidy
	standard-version

lsif:
	go run github.com/sourcegraph/lsif-go/cmd/lsif-go@${LSIF_GO_VERSION}

junit:
	go test -covermode=count ./... -v 2>&1 | go run github.com/jstemmer/go-junit-report@${JUNIT_VERSION} -set-exit-code > report.xml
