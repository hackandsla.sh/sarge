package sarge

import (
	"fmt"
	"sort"
	"strings"
)

const lenPadding = 2
const alignmentPadding = 3
const lenShortFlagPrefix = 4

func (p *Parser) Help() string {
	var help strings.Builder

	p.initFlags()
	p.setCommandName()

	if p.description != "" {
		fmt.Fprintln(&help)

		fmt.Fprintln(&help, p.description)
	}

	fmt.Fprintln(&help)
	fmt.Fprintln(&help, "Usage:")
	fmt.Fprintf(&help, "%s%s [flags]\n", padding(lenPadding), p.commandName)

	fmt.Fprintln(&help)
	fmt.Fprintln(&help, "Flags:")

	args := p.argsList()

	args = append(args, arg{
		shortFlag: 'h',
		flag:      "help",
		help:      "View this help text",
	})

	if p.version != "" {
		args = append(args, arg{
			flag: "version",
			help: "Print version information",
		})
	}

	var flagStrings []string
	for _, arg := range args {
		flagStrings = append(flagStrings, arg.flagString())
	}

	maxStringSize := getMaxStringSize(flagStrings)

	for i, flagString := range flagStrings {
		columnPadding := maxStringSize + alignmentPadding - len(flagString)
		fmt.Fprintf(&help, "%s%s%s%s", padding(lenPadding), flagString, padding(columnPadding), args[i].help)

		if args[i].value != nil && args[i].value.String() != "" {
			fmt.Fprintf(&help, " (default %s)", args[i].value.String())
		}

		fmt.Fprintln(&help)
	}

	return help.String()
}

func (a *arg) flagString() string {
	var s strings.Builder

	if a.shortFlag != 0 {
		fmt.Fprintf(&s, "-%c, ", a.shortFlag)
	} else {
		fmt.Fprint(&s, padding(lenShortFlagPrefix))
	}

	fmt.Fprintf(&s, "--%s", a.flag)

	return s.String()
}

func (p *Parser) argsList() []arg {
	args := make([]arg, 0, len(p.args))
	for _, a := range p.args {
		args = append(args, a)
	}

	sort.Slice(args, func(i, j int) bool {
		return args[i].flag < args[j].flag
	})

	return args
}

func (p *Parser) Version() string {
	p.setCommandName()

	return fmt.Sprintf("%s %s\n", p.commandName, p.version)
}

func getMaxStringSize(ss []string) int {
	var max int

	for _, s := range ss {
		if len(s) > max {
			max = len(s)
		}
	}

	return max
}

func padding(n int) string {
	return strings.Repeat(" ", n)
}
