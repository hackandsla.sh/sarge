#!/bin/bash

# tidy.sh helps with keeping the go module files clean. Since goreleaser is
# picky about having a clean git repository, we need to ensure that go.mod and
# go.sum are in a tidied state before getting pushed to origin.
#
# This script does this by resetting the current repo (which should unstage any
# staged files, if any) and attempting to tidy the module. If it detects at
# least one difference in the repository state from before doing the `go mod
# tidy` and after doing it, that means that we need to commit changes to the
# relevant files

git reset
go mod tidy
git add go.mod
git add go.sum
NUM_STAGED=$(expr $(git status --porcelain 2>/dev/null| grep "^M" | wc -l))
if (( NUM_STAGED > 0 )); then 
    git commit -m "chore(go.mod): Tidy go module files"
fi 