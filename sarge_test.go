package sarge_test

import (
	"fmt"
	"os"
	"path/filepath"
	"testing"

	"gitlab.com/hackandsla.sh/sarge"
)

func TestParser_Unmarshal(t *testing.T) {
	t.Run("Allows using short flags", func(t *testing.T) {
		// Arrange
		type Args struct {
			Foo string
		}

		var args Args

		parser := sarge.New(&args)
		parser.String(&args.Foo,
			sarge.WithShortFlag('f'),
		)

		setOSArgs(t, "-f", "bar")

		// Act
		err := parser.Execute()

		// Assert
		assertNilErr(t, err)
		assertEquals(t, "bar", args.Foo)
	})

	t.Run("Allows overriding the default flag name", func(t *testing.T) {
		// Arrange
		type Args struct {
			Foo string
		}

		var args Args

		parser := sarge.New(&args)
		parser.String(&args.Foo,
			sarge.WithFlag("different-flag"),
		)

		setOSArgs(t, "--different-flag", "bar")

		// Act
		err := parser.Execute()

		// Assert
		assertNilErr(t, err)
		assertEquals(t, "bar", args.Foo)
	})

	t.Run("Allows registering custom help text", func(t *testing.T) {
		// Arrange
		type Args struct {
			Foo string
		}

		var args Args

		parser := sarge.New(&args)
		parser.String(&args.Foo,
			sarge.WithHelp("Some help message"),
		)

		setOSArgs(t)

		// Act
		err := parser.Execute()

		// Assert
		assertNilErr(t, err)
	})

	t.Run("Allows using a pointer value in the args struct", func(t *testing.T) {
		// Arrange
		type Args struct {
			Foo *string
		}

		var args Args

		parser := sarge.New(&args)

		setOSArgs(t, "--foo", "bar")

		// Act
		err := parser.Execute()

		// Assert
		assertNilErr(t, err)
		assertEquals(t, "bar", *args.Foo)
	})

	t.Run("Allows using a non-pointer value in the args struct", func(t *testing.T) {
		// Arrange
		type Args struct {
			Foo string
		}

		var args Args

		parser := sarge.New(&args)

		setOSArgs(t, "--foo", "bar")

		// Act
		err := parser.Execute()

		// Assert
		assertNilErr(t, err)
		assertEquals(t, "bar", args.Foo)
	})

	t.Run("Allows using a default value", func(t *testing.T) {
		// Arrange
		type Args struct {
			Foo string
		}

		var args Args
		args.Foo = "bar"

		parser := sarge.New(&args)

		setOSArgs(t)

		// Act
		err := parser.Execute()

		// Assert
		assertNilErr(t, err)
		assertEquals(t, "bar", args.Foo)
	})

	t.Run("Allows using = symbol between flag and value", func(t *testing.T) {
		// Arrange
		type Args struct {
			Foo string
		}

		var args Args

		parser := sarge.New(&args)

		setOSArgs(t, "--foo=bar")

		// Act
		err := parser.Execute()

		// Assert
		assertNilErr(t, err)
		assertEquals(t, "bar", args.Foo)
	})

	t.Run("Errors if a flag isn't defined", func(t *testing.T) {
		// Arrange
		type Args struct {
			Foo string
		}

		var args Args

		parser := sarge.New(&args)

		setOSArgs(t, "--bar", "baz")

		// Act
		err := parser.Execute()

		// Assert
		assertErrIs(t, sarge.ErrUnknownFlag, err)
	})

	t.Run("Errors if a flag value isn't defined", func(t *testing.T) {
		// Arrange
		type Args struct {
			Foo string
		}

		var args Args

		parser := sarge.New(&args)

		setOSArgs(t, "--foo")

		// Act
		err := parser.Execute()

		// Assert
		assertErrIs(t, sarge.ErrMissingValue, err)
	})

	t.Run("Errors if a flag value isn't defined on a middle flag", func(t *testing.T) {
		// Arrange
		type Args struct {
			Foo string
			Bar string
		}

		var args Args

		parser := sarge.New(&args)

		setOSArgs(t, "--foo", "--bar", "baz")

		// Act
		err := parser.Execute()

		// Assert
		assertErrIs(t, sarge.ErrMissingValue, err)
	})

	t.Run("Allows using a substruct within the args struct", func(t *testing.T) {
		// Arrange
		type Args struct {
			Foo struct {
				Bar string
			}
		}

		var args Args

		parser := sarge.New(&args)

		setOSArgs(t, "--foo-bar", "bar")

		// Act
		err := parser.Execute()

		// Assert
		assertNilErr(t, err)
		assertEquals(t, "bar", args.Foo.Bar)
	})

	t.Run("Allows using a pointer to a sub-struct within the args struct", func(t *testing.T) {
		// Arrange
		type Foo struct {
			Bar string
		}

		type Args struct {
			Foo *Foo
		}

		var args Args

		parser := sarge.New(&args)

		setOSArgs(t, "--foo-bar", "bar")

		// Act
		err := parser.Execute()

		// Assert
		assertNilErr(t, err)
		assertEquals(t, "bar", args.Foo.Bar)
	})

	t.Run("Automatically parses env vars", func(t *testing.T) {
		// Arrange
		type Args struct {
			Foo string
		}

		var args Args

		parser := sarge.New(&args)

		t.Setenv("FOO", "bar")

		setOSArgs(t)

		// Act
		err := parser.Execute()

		// Assert
		assertNilErr(t, err)
		assertEquals(t, "bar", args.Foo)
	})

	t.Run("When given both command-line args and env vars, prefers command-line args", func(t *testing.T) {
		// Arrange
		type Args struct {
			Foo string
		}

		var args Args

		parser := sarge.New(&args)

		t.Setenv("FOO", "bar")

		setOSArgs(t, "--foo", "baz")

		// Act
		err := parser.Execute()

		// Assert
		assertNilErr(t, err)
		assertEquals(t, "baz", args.Foo)
	})

	t.Run("When given both env vars and default values, prefers the env vars", func(t *testing.T) {
		// Arrange
		type Args struct {
			Foo string
		}

		var args Args
		args.Foo = "baz"

		parser := sarge.New(&args)

		t.Setenv("FOO", "bar")

		setOSArgs(t)

		// Act
		err := parser.Execute()

		// Assert
		assertNilErr(t, err)
		assertEquals(t, "bar", args.Foo)
	})

	t.Run("Allows registering a custom env var prefix", func(t *testing.T) {
		// Arrange
		type Args struct {
			Foo string
		}

		var args Args

		parser := sarge.New(&args,
			sarge.WithEnvPrefix("TEST"),
		)

		t.Setenv("TEST_FOO", "bar")

		setOSArgs(t)

		// Act
		err := parser.Execute()

		// Assert
		assertNilErr(t, err)
		assertEquals(t, "bar", args.Foo)
	})

	t.Run("Errors when given an invalid command-line argument", func(t *testing.T) {
		// Arrange
		type Args struct {
			Foo int
		}

		var args Args
		parser := sarge.New(&args)

		setOSArgs(t, "--foo", "not-an-int")

		// Act
		err := parser.Execute()

		// Assert
		assertErrIs(t, sarge.ErrInvalidValue, err)
	})

	t.Run("Errors when given an invalid env var", func(t *testing.T) {
		// Arrange
		type Args struct {
			Foo int
		}

		var args Args
		parser := sarge.New(&args)

		t.Setenv("FOO", "not-an-int")

		// Act
		err := parser.Execute()

		// Assert
		assertErrIs(t, sarge.ErrInvalidValue, err)
	})

	t.Run("Allows using custom marshalers", func(t *testing.T) {
		// Arrange
		type Args struct {
			Foo string `json:"foo,omitempty"`
		}

		var args Args

		setOSArgs(t)

		file := filepath.Join(t.TempDir(), "config.json")

		if err := os.WriteFile(file, []byte("{\"foo\": \"bar\"}"), 0600); err != nil {
			t.Fatalf("error creating file '%s': %v", file, err)
		}

		// Act
		parser := sarge.New(
			&args,
			sarge.WithUnmarshaler(&JSONConfig{file}),
		)
		err := parser.Execute()

		// Assert
		assertNilErr(t, err)
		assertEquals(t, "bar", args.Foo)
	})

	t.Run("Custom unmarshalers don't affect defaults", func(t *testing.T) {
		// Arrange
		type Args struct {
			Foo string `json:"foo,omitempty"`
		}

		var args Args

		setOSArgs(t, "-h")

		file := filepath.Join(t.TempDir(), "config.json")

		if err := os.WriteFile(file, []byte("{\"foo\": \"bar\"}"), 0600); err != nil {
			t.Fatalf("error creating file '%s': %v", file, err)
		}

		// Act
		parser := sarge.New(
			&args,
			sarge.WithUnmarshaler(&JSONConfig{file}),
		)
		err := parser.Execute()

		// Assert
		assertErrIs(t, sarge.ErrHelp, err)
		assertEquals(t, "", args.Foo)
	})

	t.Run("Errors result in the original struct being unmodified", func(t *testing.T) {
		// Arrange
		type Args struct {
			Foo int
		}

		var args Args

		args.Foo = 42
		setOSArgs(t, "--foo", "2", "--flag-does-not-exist")

		// Act
		parser := sarge.New(&args)
		err := parser.Execute()

		// Assert
		assertErrIs(t, sarge.ErrUnknownFlag, err)
		assertEquals(t, 42, args.Foo)
	})

	t.Run("Prefers env vars over custom marshalers", func(t *testing.T) {
		// Arrange
		type Args struct {
			Foo string
		}

		var args Args

		file := filepath.Join(t.TempDir(), "config.json")

		setOSArgs(t)

		if err := os.WriteFile(file, []byte("{\"foo\": \"bar\"}"), 0600); err != nil {
			t.Fatalf("error creating file '%s': %v", file, err)
		}
		t.Setenv("FOO", "baz")

		// Act
		parser := sarge.New(
			&args,
			sarge.WithUnmarshaler(&JSONConfig{file}),
		)
		err := parser.Execute()

		// Assert
		assertNilErr(t, err)
		assertEquals(t, "baz", args.Foo)
	})

	t.Run("Prefers values from custom marshalers over default values", func(t *testing.T) {
		// Arrange
		type Args struct {
			Foo string
		}

		var args Args

		setOSArgs(t)

		file := filepath.Join(t.TempDir(), "config.json")

		if err := os.WriteFile(file, []byte("{\"foo\": \"baz\"}"), 0600); err != nil {
			t.Fatalf("error creating file '%s': %v", file, err)
		}

		// Act
		args.Foo = "bar"
		parser := sarge.New(
			&args,
			sarge.WithUnmarshaler(&JSONConfig{file}),
		)
		err := parser.Execute()

		// Assert
		assertNilErr(t, err)
		assertEquals(t, "baz", args.Foo)
	})
}

func TestParser_New(t *testing.T) {
	t.Run("Panics when given a non-pointer", func(t *testing.T) {
		// Arrange
		defer func() { _ = recover() }()

		type Args struct {
			Foo string
		}

		// Act
		_ = sarge.New(Args{})

		// Assert
		t.Errorf("code did not panic")
	})

	t.Run("Panics when given a non-struct-pointer", func(t *testing.T) {
		// Arrange
		defer func() { _ = recover() }()

		var args string

		// Act
		_ = sarge.New(&args)

		// Assert
		t.Errorf("code did not panic")
	})

	t.Run("Automatically registers any values implementing the Value interface", func(t *testing.T) {
		// Arrange
		type Args struct {
			Foo *customValue
		}

		var args Args

		parser := sarge.New(&args)

		setOSArgs(t, "--foo", "bar")

		// Act
		err := parser.Execute()

		// Assert
		assertNilErr(t, err)
		assertEquals(t, &customValue{"BAR"}, args.Foo)
	})

	t.Run("Automatically registers any values whose pointer types implement the Value interface", func(t *testing.T) {
		// Arrange
		type Args struct {
			Foo customValue
		}

		var args Args

		parser := sarge.New(&args)

		setOSArgs(t, "--foo", "bar")

		// Act
		err := parser.Execute()

		// Assert
		assertNilErr(t, err)
		assertEquals(t, customValue{"BAR"}, args.Foo)
	})
}

func TestParser_Options(t *testing.T) {
	t.Run("Panics when given a non-pointer", func(t *testing.T) {
		// Arrange
		type Args struct {
			Foo string
		}

		var args Args
		parser := sarge.New(&args)

		defer func() { _ = recover() }()

		// Act
		parser.Options(args.Foo)

		// Assert
		t.Errorf("code did not panic")
	})

	t.Run("Panics when given a pointer outside the original args struct", func(t *testing.T) {
		// Arrange
		type Args struct {
			Foo string
		}

		var args Args
		parser := sarge.New(&args)

		var otherArgs Args

		defer func() { _ = recover() }()

		// Act
		parser.Options(&otherArgs.Foo)

		// Assert
		t.Errorf("code did not panic")
	})
}

func TestParser_Sub(t *testing.T) {
	t.Run("Allows using sub-commands", func(t *testing.T) {
		// Arrange
		type Sub1Args struct {
			Foo string
		}

		type Sub2Args struct {
			Bar string
		}

		parser := sarge.New(new(struct{}))

		setOSArgs(t, "sub1", "-f", "bar")

		// Act
		var sub1Args Sub1Args
		var sub1Called bool
		cmdSub1 := parser.Sub("sub1", &sub1Args, sarge.WithFunc(func() error {
			sub1Called = true

			return nil
		}))
		cmdSub1.String(&sub1Args.Foo, sarge.WithShortFlag('f'))

		var sub2Args Sub2Args
		var sub2Called bool
		cmdSub2 := parser.Sub("sub2", &sub2Args, sarge.WithFunc(func() error {
			sub2Called = true

			return nil
		}))
		cmdSub2.String(&sub2Args.Bar, sarge.WithShortFlag('b'))

		err := parser.Execute()

		// Assert
		assertNilErr(t, err)

		assertEquals(t, true, sub1Called)
		assertEquals(t, "bar", sub1Args.Foo)

		assertEquals(t, false, sub2Called)
		assertEquals(t, "", sub2Args.Bar)
	})

	t.Run("Can use multiple layers of sub-commands", func(t *testing.T) {
		// Arrange
		type Args struct {
			Foo string
		}

		parser := sarge.New(new(struct{}))

		setOSArgs(t, "foo", "bar", "-f", "bar")

		// Act
		var args Args
		var cmdSub2Called bool
		cmdSub1 := parser.Sub("foo", new(struct{}))
		cmdSub2 := cmdSub1.Sub("bar", &args, sarge.WithFunc(func() error {
			cmdSub2Called = true

			return nil
		}))
		cmdSub2.String(&args.Foo, sarge.WithShortFlag('f'))

		err := parser.Execute()

		// Assert
		assertNilErr(t, err)

		assertEquals(t, true, cmdSub2Called)
		assertEquals(t, "bar", args.Foo)
	})

	t.Run("Errors if an invalid subcommand is used", func(t *testing.T) {
		// Arrange
		type Args struct {
			Foo string
		}

		parser := sarge.New(new(struct{}))

		setOSArgs(t, "bar")

		// Act
		var args Args
		var subCalled bool
		_ = parser.Sub("foo", &args, sarge.WithFunc(func() error {
			subCalled = true

			return nil
		}))

		err := parser.Execute()

		// Assert
		assertErrIs(t, sarge.ErrSubcommandNotFound, err)

		assertEquals(t, false, subCalled)
		assertEquals(t, "", args.Foo)
	})
}

func ExampleParser() {
	type Args struct {
		Name string
		Port int
		DB   struct {
			Username string
			Password string
		}
	}

	var args Args
	// Set defaults by assigning them to the struct.
	args.Port = 8080
	args.DB.Username = "myapp"

	// Initialize parser using a pointer to args. You can also specify
	// parser-level settings.
	p := sarge.New(&args,
		sarge.WithEnvPrefix("EXAMPLE"),
	)

	// Configure each argument with various settings.
	p.String(&args.Name,
		sarge.WithShortFlag('n'),
		sarge.WithHelp("The name of your thing"),
	)
	p.Int(&args.Port,
		sarge.WithHelp("The port to listen on"),
	)

	// Set values through environment.
	os.Setenv("EXAMPLE_AGE", "42")
	os.Setenv("EXAMPLE_NOTIFICATION_DURATION", "10m")

	// Set values through command line arguments
	os.Args = []string{"myprognam", "-n", "Bob", "--db-password", "S3cret!"}

	err := p.Execute()
	if err != nil {
		panic(err)
	}

	fmt.Printf("Name: %s\n", args.Name)
	fmt.Printf("Port: %d\n", args.Port)
	fmt.Printf("DB.Username: %s\n", args.DB.Username)
	fmt.Printf("DB.Password: %s\n", args.DB.Password)

	// Output:
	// Name: Bob
	// Port: 8080
	// DB.Username: myapp
	// DB.Password: S3cret!
}
