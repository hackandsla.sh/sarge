package sarge

import (
	"encoding"
	"errors"
	"fmt"
	"io"
	"os"
	"path/filepath"
	"reflect"
	"strings"
	"time"
)

//nolint:gochecknoglobals // These are used for monkey-patching tests
var (
	osExit             = os.Exit
	osStdout io.Writer = os.Stdout
	osStderr io.Writer = os.Stderr
)

// defaultTypeHandlers defines all the value mappings for our default type set,
// like primitive types and well-known types from the standard library.
func defaultTypeHandlers() []ParserOption {
	return []ParserOption{
		withValueMapper(new(string), newStringValue),
		withValueMapper(new(int), newIntValue),
		withValueMapper(new(time.Duration), newDurationValue),
	}
}

type arg struct {
	help      string
	flag      string
	shortFlag rune

	structPath   []string
	addr         uintptr
	value        Value
	reflectValue reflect.Value
	defaultValue reflect.Value
}

// Value is an interface that allows registering custom types into the parser.
type Value interface {
	fmt.Stringer
	encoding.TextUnmarshaler
}

type valueMapperFunc func(v interface{}) Value

type Parser struct {
	args            map[uintptr]arg
	argsStruct      interface{}
	registeredTypes map[reflect.Type]valueMapperFunc
	envPrefix       string
	unmarshalers    []Unmarshaler
	subCmds         map[string]subCmd
	fn              func() error
	description     string
	version         string
	commandName     string
}

func New(pointer interface{}, options ...ParserOption) *Parser {
	if !isStructPointer(pointer) {
		panic("a struct pointer must be given")
	}

	p := &Parser{
		args:            map[uintptr]arg{},
		argsStruct:      pointer,
		registeredTypes: map[reflect.Type]valueMapperFunc{},
		subCmds:         map[string]subCmd{},
	}

	options = append(defaultTypeHandlers(), options...)

	for _, option := range options {
		p = option(p)
	}

	p.walkStruct(reflect.ValueOf(pointer), []string{})

	return p
}

type subCmd struct {
	name   string
	parser *Parser
}

func (p *Parser) Sub(name string, args interface{}, options ...ParserOption) *Parser {
	sub := subCmd{
		name:   name,
		parser: New(args, options...),
	}

	p.subCmds[name] = sub

	return sub.parser
}

type ParserOption func(p *Parser) *Parser

func WithEnvPrefix(prefix string) ParserOption {
	return func(p *Parser) *Parser {
		p.envPrefix = prefix
		return p
	}
}

func WithUnmarshaler(u Unmarshaler) ParserOption {
	return func(p *Parser) *Parser {
		p.unmarshalers = append(p.unmarshalers, u)
		return p
	}
}

func WithFunc(fn func() error) ParserOption {
	return func(p *Parser) *Parser {
		p.fn = fn
		return p
	}
}

func WithDescription(d string) ParserOption {
	return func(p *Parser) *Parser {
		p.description = d
		return p
	}
}

func WithVersion(v string) ParserOption {
	return func(p *Parser) *Parser {
		p.version = v
		return p
	}
}

// withValueMapper allows registering a custom value type. While `New`
// automatically picks up everything implementing the `Value` interface, there
// are some cases where we want to implement `Value` on things outside our
// control (e.g. primitive types, or types defined in external packages). The
// only way to do so is by registering the type along with a mapping function,
// which converts the type into a `Value` implementer that we can control.
func withValueMapper(typ interface{}, handler valueMapperFunc) ParserOption {
	return func(p *Parser) *Parser {
		v := reflect.ValueOf(typ)

		// If given a pointer type, ensure that we register the type itself. This is
		// syntactic sugar allowing us to use the `new` function more easily, since
		// it always returns a pointer type to the underlying concrete type.
		if v.Kind() == reflect.Ptr {
			v = v.Elem()
		}

		p.registeredTypes[v.Type()] = handler

		return p
	}
}

func (p *Parser) Options(v interface{}, options ...ArgOption) {
	if !isPointer(v) {
		panic("given field is not a pointer")
	}

	addr := reflect.ValueOf(v).Pointer()

	a, ok := p.args[addr]
	if !ok {
		panic("the given pointer doesn't point to a member within the struct")
	}

	for _, option := range options {
		a = option(a)
	}

	p.args[addr] = a
}

type ArgOption func(a arg) arg

func WithShortFlag(flag rune) ArgOption {
	return func(a arg) arg {
		a.shortFlag = flag
		return a
	}
}

func WithHelp(help string) ArgOption {
	return func(a arg) arg {
		a.help = help
		return a
	}
}

func WithFlag(flag string) ArgOption {
	return func(a arg) arg {
		a.flag = flag
		return a
	}
}

func (p *Parser) Execute() error {
	p.setCommandName()

	args := os.Args[1:]

	return p.execute(args)
}

func (p *Parser) MustExecute() {
	err := p.Execute()
	if errors.Is(err, ErrHelp) {
		fmt.Fprint(osStdout, p.Help())

		osExit(0)

		return
	}

	if errors.Is(err, ErrVersionRequested) {
		fmt.Fprint(osStdout, p.Version())

		osExit(0)

		return
	}

	if err != nil {
		fmt.Fprint(osStdout, p.Help())

		fmt.Fprintf(osStderr, "error: %v\n", err)

		osExit(1)

		return
	}
}

type Unmarshaler interface {
	Unmarshal(args interface{}) error
}

func (p *Parser) execute(args []string) (err error) {
	if len(args) > 0 && !strings.HasPrefix(args[0], "-") {
		// The first arg isn't a prefix; search for a relevant sub command
		sub, ok := p.subCmds[args[0]]
		if !ok {
			return fmt.Errorf("%w: %s", ErrSubcommandNotFound, args[0])
		}

		return sub.parser.execute(args[1:])
	}

	p.initFlags()

	p.saveDefaults()

	defer func() {
		if err != nil {
			p.restoreDefaults()
		}
	}()

	if err1 := p.doCustomUnmarshals(); err1 != nil {
		return err1
	}

	if err1 := p.parseEnv(); err1 != nil {
		return err1
	}

	err = p.processArgs(args)
	if err != nil {
		return err
	}

	if p.fn != nil {
		return p.fn()
	}

	return nil
}

func (p *Parser) doCustomUnmarshals() error {
	for _, unmarshaler := range p.unmarshalers {
		if err := unmarshaler.Unmarshal(p.argsStruct); err != nil {
			return fmt.Errorf("error during unmarshaling: %w", err)
		}
	}

	return nil
}

func (p *Parser) initFlags() {
	for k, arg := range p.args {
		if arg.flag == "" {
			arg.flag = pathToFlag(arg.structPath)
		}

		p.args[k] = arg
	}
}

func (p *Parser) saveDefaults() {
	for k, arg := range p.args {
		arg.defaultValue = deepCopy(arg.reflectValue)

		p.args[k] = arg
	}
}

func (p *Parser) restoreDefaults() {
	for _, arg := range p.args {
		arg.reflectValue.Set(arg.defaultValue)
	}
}

func (p *Parser) parseEnv() error {
	for _, arg := range p.args {
		envVar := getEnvVar(p.envPrefix, arg.structPath)
		if envValue, ok := os.LookupEnv(envVar); ok {
			if err := arg.value.UnmarshalText([]byte(envValue)); err != nil {
				return fmt.Errorf("%w: %v", ErrInvalidValue, err)
			}
		}
	}

	return nil
}

// processArgs goes through arguments one-by-one, parses them, and assigns the result to
// the underlying struct field.
func (p *Parser) processArgs(args []string) error {
	// must use explicit for loop, not range, because we manipulate i inside the loop
	for i := 0; i < len(args); i++ {
		arg := args[i]

		if arg == "-h" || arg == "--help" {
			return ErrHelp
		}

		if p.version != "" && arg == "--version" {
			return ErrVersionRequested
		}

		var value string

		// TODO add support for combined single boolean flags (e.g. -xvf)
		opt := strings.TrimLeft(arg, "-")

		// check for an equals sign, as in "--foo=bar"
		if pos := strings.Index(opt, "="); pos != -1 {
			value = opt[pos+1:]
			opt = opt[:pos]
		}

		spec, err := p.findOption(opt)
		if err != nil {
			return err
		}

		// if we have something like "--foo" then the value is the next argument
		if value == "" {
			if i+1 == len(args) {
				return fmt.Errorf("%w: %s", ErrMissingValue, arg)
			}

			if isFlag(args[i+1]) {
				return fmt.Errorf("%w: %s", ErrMissingValue, arg)
			}

			value = args[i+1]
			i++
		}

		if err := spec.value.UnmarshalText([]byte(value)); err != nil {
			return fmt.Errorf("%w for flag %s: %s", ErrInvalidValue, arg, value)
		}
	}

	return nil
}

func (p *Parser) findOption(opt string) (arg, error) {
	for _, a := range p.args {
		if strings.EqualFold(a.flag, opt) || strings.EqualFold(string(a.shortFlag), opt) {
			return a, nil
		}
	}

	return arg{}, fmt.Errorf("%w: %s", ErrUnknownFlag, opt)
}

func (p *Parser) walkStruct(v reflect.Value, path []string) {
	if !v.IsValid() || !v.CanInterface() {
		return
	}

	vKind := v.Kind()

	if canElem(v) {
		// If we hit any nil pointers, ensure they're initialized so that we don't
		// try recursing down into nil values.
		initNilPointer(v)

		v = v.Elem()
		p.walkStruct(v, path)
	}

	// If we hit a type that is registered in our value mappers, stop and use it
	// as an arg.
	if valueMapperFn, ok := p.registeredTypes[v.Type()]; ok {
		addr := v.Addr().Pointer()
		p.args[addr] = arg{
			structPath:   path,
			addr:         addr,
			reflectValue: v,
			value:        valueMapperFn(v.Addr().Interface()),
		}

		return
	}

	// If we hit anything implementing our `Value` interface, stop and use it as
	// an arg.
	if value, ok := v.Addr().Interface().(Value); ok {
		addr := v.Addr().Pointer()
		p.args[addr] = arg{
			structPath:   path,
			addr:         addr,
			reflectValue: v,
			value:        value,
		}

		return
	}

	// Recurse down the struct tree.
	if vKind == reflect.Struct {
		for i := 0; i < v.NumField(); i++ {
			field := v.Field(i)
			fieldName := v.Type().Field(i).Name
			newPath := path
			newPath = append(newPath, fieldName)
			p.walkStruct(field, newPath)
		}
	}
}

func getEnvVar(prefix string, path []string) string {
	var upper []string

	if prefix != "" {
		upper = append(upper, prefix)
	}

	for _, p := range path {
		upper = append(upper, strings.ToUpper(p))
	}

	return strings.Join(upper, "_")
}

// isFlag returns true if a token is a flag such as "-v" or "--user" but not "-" or "--".
func isFlag(s string) bool {
	return strings.HasPrefix(s, "-") && strings.TrimLeft(s, "-") != ""
}

func pathToFlag(s []string) string {
	var path []string
	for _, s := range s {
		path = append(path, strings.ToLower(s))
	}

	return strings.Join(path, "-")
}

func (p *Parser) setCommandName() {
	p.commandName = filepath.Base(os.Args[0])
}
