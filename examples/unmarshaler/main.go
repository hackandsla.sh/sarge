package main

import (
	"errors"
	"fmt"
	"os"

	"gitlab.com/hackandsla.sh/sarge"
	yaml "gopkg.in/yaml.v2"
)

type YAMLConfig struct {
	file string
}

func (cfg *YAMLConfig) Unmarshal(args interface{}) error {
	b, err := os.ReadFile(cfg.file)
	if err != nil {
		if errors.Is(err, os.ErrNotExist) {
			return nil
		}

		return fmt.Errorf("error reading file '%s': %w", cfg.file, err)
	}

	return yaml.Unmarshal(b, args)
}

func main() {
	type Args struct {
		Name string `yaml:"name,omitempty"`
		Port int    `yaml:"port,omitempty"`
		DB   struct {
			Username string `yaml:"username,omitempty"`
			Password string `yaml:"password,omitempty"`
		} `yaml:"db,omitempty"`
	}

	var args Args

	// Create a new sarge parser.
	p := sarge.New(&args,
		sarge.WithUnmarshaler(&YAMLConfig{file: "./config.yaml"}),
	)

	// Unmarshal values from the environment into our struct.
	p.MustExecute()

	// Use the configuration struct.
	fmt.Printf("Name: %s\n", args.Name)
	fmt.Printf("Port: %d\n", args.Port)
	fmt.Printf("DB.Username: %s\n", args.DB.Username)
	fmt.Printf("DB.Password: %s\n", args.DB.Password)
}
