package main

import (
	"fmt"
	"strings"

	"gitlab.com/hackandsla.sh/sarge"
)

type UppercaseString struct {
	s string
}

func (us *UppercaseString) UnmarshalText(b []byte) error {
	us.s = strings.ToUpper(string(b))
	return nil
}

func (us *UppercaseString) String() string {
	return us.s
}

var _ sarge.Value = (*UppercaseString)(nil)

func main() {
	type Args struct {
		// Use our custom type that implements sarge.Value
		Name UppercaseString
	}

	var args Args

	p := sarge.New(&args)

	p.MustExecute()

	fmt.Printf("Name: %s\n", args.Name.String())
}
