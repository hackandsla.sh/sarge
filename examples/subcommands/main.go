package main

import (
	"fmt"

	"gitlab.com/hackandsla.sh/sarge"
)

func main() {
	type DBConfig struct {
		Username string
		Password string
	}

	type Args struct {
		Name string
		Port int
		DB   DBConfig
	}

	var args Args

	p := sarge.New(&args, sarge.WithFunc(func() error {
		fmt.Println("In top-level command")
		fmt.Println("----")
		fmt.Printf("Name: %s\n", args.Name)
		fmt.Printf("Port: %d\n", args.Port)
		fmt.Printf("DB.Username: %s\n", args.DB.Username)
		fmt.Printf("DB.Password: %s\n", args.DB.Password)

		return nil
	}))

	var migrateArgs DBConfig

	p.Sub("migrate", &migrateArgs, sarge.WithFunc(func() error {
		fmt.Println("In migrate subcommand")
		fmt.Println("----")
		fmt.Printf("Username: %s\n", migrateArgs.Username)
		fmt.Printf("Password: %s\n", migrateArgs.Password)
		fmt.Println()

		return nil
	}))

	p.MustExecute()
}
