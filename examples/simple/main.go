package main

import (
	"fmt"

	"gitlab.com/hackandsla.sh/sarge"
)

func main() {
	// Define our app's configuration struct.
	type Args struct {
		Name string
		Port int
		DB   struct {
			Username string
			Password string
		}
	}

	var args Args

	// Create a new sarge parser.
	p := sarge.New(&args)

	// Unmarshal values from the environment into our struct.
	p.MustExecute()

	// Use the configuration struct.
	fmt.Printf("Name: %s\n", args.Name)
	fmt.Printf("Port: %d\n", args.Port)
	fmt.Printf("DB.Username: %s\n", args.DB.Username)
	fmt.Printf("DB.Password: %s\n", args.DB.Password)
}
