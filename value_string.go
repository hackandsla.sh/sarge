package sarge

func (p *Parser) String(s *string, options ...ArgOption) {
	p.Options(s, options...)
}

type stringValue struct {
	ptr *string
}

func newStringValue(v interface{}) Value {
	ptr := v.(*string)
	return &stringValue{ptr}
}

func (s *stringValue) UnmarshalText(b []byte) error {
	*s.ptr = string(b)
	return nil
}

func (s *stringValue) String() string { return *s.ptr }
