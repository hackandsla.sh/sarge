package sarge

import "errors"

var (
	ErrSubcommandNotFound = errors.New("subcommand not found")
	ErrHelp               = errors.New("help requested")
	ErrVersionRequested   = errors.New("version requested")
	ErrUnknownFlag        = errors.New("flag not found")
	ErrMissingValue       = errors.New("missing value")
	ErrInvalidValue       = errors.New("invalid value")
)
