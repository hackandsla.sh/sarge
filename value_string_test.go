package sarge_test

import (
	"testing"

	"gitlab.com/hackandsla.sh/sarge"
)

func TestParser_Unmarshal_string(t *testing.T) {
	// Arrange
	type Args struct {
		Foo string
	}

	var args Args

	parser := sarge.New(&args)

	setOSArgs(t, "--foo", "bar")

	// Act
	parser.String(&args.Foo)
	err := parser.Execute()

	// Assert
	assertNilErr(t, err)
	assertEquals(t, "bar", args.Foo)
}
